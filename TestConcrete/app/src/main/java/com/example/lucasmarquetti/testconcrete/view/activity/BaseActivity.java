package com.example.lucasmarquetti.testconcrete.view.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Browser;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.lucasmarquetti.testconcrete.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by LucasMarquetti on 11/11/16.
 */

public class BaseActivity extends AppCompatActivity {
    private ProgressDialog mProgressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    public void showProgress() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                dismissProgress();
            }
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void dismissProgress() {
        if (mProgressDialog != null) {
            try {
                mProgressDialog.dismiss();
            } catch (Exception e) {
            }
            mProgressDialog = null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

package com.example.lucasmarquetti.testconcrete.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by LucasMarquetti on 11/11/16.
 */

public class PullRequest implements Parcelable {

    @SerializedName("id")
    private int id;

    @SerializedName("html_url")
    private String urlPullRequest;

    @SerializedName("number")
    private int number;

    @SerializedName("title")
    private String title;

    @SerializedName("body")
    private String body;

    @SerializedName("created_at")
    private String createAt;//"2016-11-11T21:13:31Z"

    @SerializedName("user")
    private Owner user;


    public PullRequest(int id, String url, int number, String title, String body, String createAt, Owner user) {
        this.id = id;
        this.urlPullRequest = url;
        this.number = number;
        this.title = title;
        this.body = body;
        this.createAt = createAt;
        this.user = user;
    }

    public PullRequest(Parcel in) {
        id = in.readInt();
        urlPullRequest = in.readString();
        number = in.readInt();
        title = in.readString();
        body = in.readString();
        createAt = in.readString();
        user = in.readParcelable(Owner.class.getClassLoader());
    }

    public static final Creator<PullRequest> CREATOR = new Creator<PullRequest>() {
        @Override
        public PullRequest createFromParcel(Parcel in) {
            return new PullRequest(in);
        }

        @Override
        public PullRequest[] newArray(int size) {
            return new PullRequest[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrlPullRequest() {
        return urlPullRequest;
    }

    public void setUrlPullRequest(String urlPullRequest) {
        this.urlPullRequest = urlPullRequest;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public Owner getUser() {
        return user;
    }

    public void setUser(Owner user) {
        this.user = user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(urlPullRequest);
        parcel.writeInt(number);
        parcel.writeString(title);
        parcel.writeString(body);
        parcel.writeString(createAt);
        parcel.writeParcelable(user, i);
    }
}

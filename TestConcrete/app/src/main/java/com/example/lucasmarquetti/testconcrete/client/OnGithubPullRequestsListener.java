package com.example.lucasmarquetti.testconcrete.client;

import com.example.lucasmarquetti.testconcrete.model.PullRequest;
import com.example.lucasmarquetti.testconcrete.model.Repository;

import java.util.List;

/**
 * Created by LucasMarquetti on 12/11/16.
 */

public interface OnGithubPullRequestsListener {

    void onListPullRequests(List<PullRequest> listPullRequests);
    void onListFail(String error);
}

package com.example.lucasmarquetti.testconcrete.view.adapter;

/**
 * Created by LucasMarquetti on 12/11/16.
 */

public interface AdapterListener {
    void onOpenPullRequests(int position);
}

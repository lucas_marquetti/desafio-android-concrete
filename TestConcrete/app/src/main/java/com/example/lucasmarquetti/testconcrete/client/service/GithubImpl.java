package com.example.lucasmarquetti.testconcrete.client.service;

import android.content.Context;

import com.example.lucasmarquetti.testconcrete.client.OnGithubPullRequestsListener;
import com.example.lucasmarquetti.testconcrete.client.OnGithubRepositoriesListener;
import com.example.lucasmarquetti.testconcrete.client.api.ApiClient;
import com.example.lucasmarquetti.testconcrete.model.PullRequest;
import com.example.lucasmarquetti.testconcrete.model.RepositoryList;

import java.util.List;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by LucasMarquetti on 12/11/16.
 */

public class GithubImpl {


    public void getListRespositories(Context context, final OnGithubRepositoriesListener listener, int page){

        Call<RepositoryList> call = ApiClient.providerAppService(context).getRepositories(page);

        call.enqueue(new Callback<RepositoryList>() {
            @Override
            public void onResponse(Call<RepositoryList> call, Response<RepositoryList> response) {
                if (response.isSuccessful()) {
                    RepositoryList repositoryList = response.body();

                    listener.onListRepositories(repositoryList.getRepositoryList());
                    return;
                }

                System.out.println("gitList Repositories fail");
                listener.onListFail("");
            }

            @Override
            public void onFailure(Call<RepositoryList> call, Throwable t) {
                System.out.println("gitList Repositories fail");
                t.printStackTrace();
                listener.onListFail("");
            }
        });

    }


    public void getListPullRequests(Context context, final OnGithubPullRequestsListener listener, String user, String nameRepository){

        Call<List<PullRequest>> call = ApiClient.providerAppService(context).getPullRequests(user, nameRepository);

        call.enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                if (response.isSuccessful()) {

                    List<PullRequest> pullRequestList = response.body();

                    listener.onListPullRequests(pullRequestList);
                    return;
                }

                System.out.println("gitList Pull requests fail");
                listener.onListFail("");
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                System.out.println("gitList Pull requests fail");
                t.printStackTrace();
                listener.onListFail("");
            }
        });

    }
}

package com.example.lucasmarquetti.testconcrete.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by LucasMarquetti on 11/11/16.
 */

public class Repository implements Parcelable {

    //    "id": 507775,
//            "name": "elasticsearch",
//            "full_name": "elastic/elasticsearch",
    //            "stargazers_count": 19276,
    //            "forks_count": 6588,
    //            "description"


    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("full_name")
    private String fullName;

    @SerializedName("stargazers_count")
    private int stargazersCount;

    @SerializedName("forks_count")
    private int forksCount;

    @SerializedName("description")
    private String description;

    @SerializedName("owner")
    private Owner owner;

    public Repository(int id, String name,  String fullName, int stargazersCount, int forksCount, String description, Owner owner) {
        this.id = id;
        this.name = name;
        this.fullName = fullName;
        this.stargazersCount = stargazersCount;
        this.forksCount = forksCount;
        this.description = description;
        this.owner = owner;
    }

    protected Repository(Parcel in) {
        id = in.readInt();
        name = in.readString();
        fullName = in.readString();
        stargazersCount = in.readInt();
        forksCount = in.readInt();
        description = in.readString();
        owner = in.readParcelable(Owner.class.getClassLoader());
    }

    public static final Creator<Repository> CREATOR = new Creator<Repository>() {
        @Override
        public Repository createFromParcel(Parcel in) {
            return new Repository(in);
        }

        @Override
        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getStargazersCount() {
        return stargazersCount;
    }

    public void setStargazersCount(int stargazersCount) {
        this.stargazersCount = stargazersCount;
    }

    public int getForksCount() {
        return forksCount;
    }

    public void setForksCount(int forksCount) {
        this.forksCount = forksCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(fullName);
        parcel.writeInt(stargazersCount);
        parcel.writeInt(forksCount);
        parcel.writeString(description);
        parcel.writeParcelable(owner, i);
    }
}

package com.example.lucasmarquetti.testconcrete.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by LucasMarquetti on 11/11/16.
 */

public class GsonUtil {

    public static Gson getGson(){
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd")
                .create();
    }
}

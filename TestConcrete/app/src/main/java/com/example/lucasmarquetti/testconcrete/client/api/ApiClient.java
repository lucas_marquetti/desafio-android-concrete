package com.example.lucasmarquetti.testconcrete.client.api;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.lucasmarquetti.testconcrete.BuildConfig;
import com.example.lucasmarquetti.testconcrete.utils.GsonUtil;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

import static okhttp3.logging.HttpLoggingInterceptor.Level.HEADERS;
import static okhttp3.logging.HttpLoggingInterceptor.Level.NONE;

/**
 * Created by LucasMarquetti on 11/11/16.
 */

public class ApiClient {

    private final static String CACHE_CONTROL = "Cache-Control";
    private static Context mContext;
    public static final String url =  "https://api.github.com";

    private ApiClient(){};

    public static AppService providerAppService(Context context)
    {
        mContext = context;
        return provideRetrofit(url).create(AppService.class);
    }

    public static Retrofit provideRetrofit (String baseUrl)
    {
        Gson gson = GsonUtil.getGson();

        return new Retrofit.Builder()
                .baseUrl( baseUrl )
                .client( provideOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    private static OkHttpClient provideOkHttpClient ()
    {
        return new OkHttpClient.Builder()
                .addInterceptor( getHttpLoggingInterceptor() )
                //.addInterceptor(getHeaderInterceptor())
                .addInterceptor( getCacheOffLineInterceptor() )
                .addNetworkInterceptor( getCacheInterceptor() )
                .cache(getCache())
                .build();
    }


    private static HttpLoggingInterceptor getHttpLoggingInterceptor ()
    {
        HttpLoggingInterceptor httpLoggingInterceptor =
                new HttpLoggingInterceptor( new HttpLoggingInterceptor.Logger()
                {
                    @Override
                    public void log (String message)
                    {
                        Timber.d( message );
                    }
                } );
        httpLoggingInterceptor.setLevel( BuildConfig.DEBUG ? HEADERS : NONE );
        return httpLoggingInterceptor;
    }


    public static Cache getCache(){

        Cache cache = null;

        try {
            cache = new Cache(new File(mContext.getCacheDir(), "http-cache" ), 10 * 1024 * 1024);

        }catch (Exception e){
            e.printStackTrace();
        }

        return cache;
    }

    private static Interceptor getHeaderInterceptor(){
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                original.url();
                Request.Builder builder = original.newBuilder();
                builder.addHeader("Content-Type", "application/json; charset=UTF-8");

                builder.method(original.method(), original.body());

                return chain.proceed(builder.build());
            }
        };
    }


    private static Interceptor getCacheInterceptor(){
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Response originalResponse = chain.proceed(chain.request());

                CacheControl cacheControl = new  CacheControl.Builder()
                        .maxAge(2, TimeUnit.MINUTES).build();


                return originalResponse.newBuilder().
                        header(CACHE_CONTROL, cacheControl.toString()).build();
            }
        };
    }


    private static Interceptor getCacheOffLineInterceptor(){
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Request request = chain.request();

                if (!checkHasNetwork(mContext)){
                    CacheControl cacheControl = new CacheControl.Builder()
                            .maxStale(7, TimeUnit.DAYS).build();

                    request = request.newBuilder().cacheControl(cacheControl).build();
               }


                return chain.proceed(request);
            }
        };
    }

    private static boolean checkHasNetwork(Context mContext)
    {
        try {
            ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();

            return networkInfo != null && networkInfo.isConnected();
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

}

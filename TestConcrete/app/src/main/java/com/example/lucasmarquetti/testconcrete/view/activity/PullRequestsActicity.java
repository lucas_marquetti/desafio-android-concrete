package com.example.lucasmarquetti.testconcrete.view.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lucasmarquetti.testconcrete.R;
import com.example.lucasmarquetti.testconcrete.client.OnGithubPullRequestsListener;
import com.example.lucasmarquetti.testconcrete.client.service.GithubImpl;
import com.example.lucasmarquetti.testconcrete.model.PullRequest;
import com.example.lucasmarquetti.testconcrete.model.Repository;
import com.example.lucasmarquetti.testconcrete.view.adapter.AdapterListener;
import com.example.lucasmarquetti.testconcrete.view.adapter.PullRequestsAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by LucasMarquetti on 11/11/16.
 */

public class PullRequestsActicity extends BaseActivity implements OnGithubPullRequestsListener, AdapterListener {

    public static final String INTENT_REPOSITORY = "repository";

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.toolbar_title)
    TextView toolbarTitle;

    @Bind(R.id.recycler_pulls)
    RecyclerView recyclerView;

    @Bind(R.id.view_empty)
    LinearLayout emptyView;

    @Bind(R.id.empty_text)
    TextView emptyTxt;

    private List<PullRequest> pullRequestList = new ArrayList<>();
    private PullRequestsAdapter mAdapter;
    public Repository repository;
    private GithubImpl service;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests);
        ButterKnife.bind(this);

        processIntent(getIntent());
        setToolbar();
    }

    private void setToolbar(){
        toolbar.setNavigationIcon(R.mipmap.ic_toolbar_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbarTitle.setText(repository.getName());
    }

    private void processIntent(Intent intent) {
        repository = intent.getParcelableExtra(INTENT_REPOSITORY);
        getPullRequests();
    }

    public void getPullRequests() {
        showProgress();
        service = new GithubImpl();
        service.getListPullRequests(this, this, repository.getOwner().getLogin(), repository.getName());
    }

    @Override
    public void onListPullRequests(List<PullRequest> listPullRequests) {
        if (listPullRequests == null || listPullRequests.size() == 0){
            setEmptyView();
            return;
        }
        pullRequestList = listPullRequests;
        createRecyclerView();
        dismissProgress();
    }

    public void setEmptyView(){
        emptyTxt.setText(getString(R.string.txt_empty_pull_requests));
        emptyView.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        dismissProgress();
    }

    public void createRecyclerView(){
        emptyView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

        if (mAdapter == null){
            mAdapter = new PullRequestsAdapter(this, this, pullRequestList);
            recyclerView.setAdapter(mAdapter);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

        }else {
            mAdapter.setNotifyDataChanged(pullRequestList);
        }

    }
    @Override
    public void onListFail(String error) {
        setEmptyView();
    }

    @Override
    public void onOpenPullRequests(int position) {
        PullRequest pullRequest = pullRequestList.get(position);

        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(pullRequest.getUrlPullRequest()));
        startActivity(i);
    }
}

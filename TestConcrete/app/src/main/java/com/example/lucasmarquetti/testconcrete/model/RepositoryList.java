package com.example.lucasmarquetti.testconcrete.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by LucasMarquetti on 12/11/16.
 */

public class RepositoryList {

    @SerializedName("items")
    private List<Repository> repositoryList;

    public List<Repository> getRepositoryList() {
        return repositoryList;
    }

    public void setRepositoryList(List<Repository> repositoryList) {
        this.repositoryList = repositoryList;
    }
}

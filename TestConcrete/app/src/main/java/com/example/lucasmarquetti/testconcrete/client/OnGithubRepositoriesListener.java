package com.example.lucasmarquetti.testconcrete.client;

import com.example.lucasmarquetti.testconcrete.model.Repository;

import java.util.List;

/**
 * Created by LucasMarquetti on 12/11/16.
 */

public interface OnGithubRepositoriesListener {

    void onListRepositories(List<Repository> listRepositories);
    void onListFail(String error);

}

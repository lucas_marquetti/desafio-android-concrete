package com.example.lucasmarquetti.testconcrete.client.api;

import com.example.lucasmarquetti.testconcrete.model.PullRequest;
import com.example.lucasmarquetti.testconcrete.model.Repository;
import com.example.lucasmarquetti.testconcrete.model.RepositoryList;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.Call;

/**
 * Created by LucasMarquetti on 11/11/16.
 */

public interface AppService {

    @GET("/search/repositories?q=language:Java&sort=stars")
    Call<RepositoryList> getRepositories(@Query("page") int page);

    @GET("/repos/{criador}/{repositorio}/pulls")
    Call<List<PullRequest>> getPullRequests(@Path("criador") String criador,
                                            @Path("repositorio") String repositorio);
}

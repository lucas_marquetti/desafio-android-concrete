package com.example.lucasmarquetti.testconcrete.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lucasmarquetti.testconcrete.R;
import com.example.lucasmarquetti.testconcrete.model.Repository;
import com.example.lucasmarquetti.testconcrete.view.activity.PullRequestsActicity;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by LucasMarquetti on 11/11/16.
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.ViewHolder> {

    private Context context;
    private List<Repository> repositories;
    private AdapterListener listener;
    private Typeface typeFace;

    public RepositoryAdapter(Context context, AdapterListener listener, List<Repository> repositories){
        this.repositories = repositories;
        this.context = context;
        this.listener = listener;

        typeFace = Typeface.createFromAsset(context.getAssets(),"fonts/icomoon.ttf");
    }

    @Override
    public RepositoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(context).inflate(R.layout.view_item_repository, parent, false);
        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Repository repository = this.repositories.get(position);

        if(repository != null){

            int sizeImage = (int) context.getResources().getDimension(R.dimen.item_icon_size);

            Picasso.with(context).
                    load(repository.getOwner().getAvatarUrl())
                    .placeholder(context.getResources().getDrawable(R.mipmap.ic_profile_gray))
                    .resize(sizeImage, sizeImage).into(holder.imgUser);
            holder.txtFork.setTypeface(typeFace);
            holder.txtStar.setTypeface(typeFace);
            holder.txtTitle.setText(repository.getName());
            holder.txtDescription.setText(repository.getDescription());
            holder.txtUsername.setText(repository.getOwner().getLogin());
            holder.txtName.setText(repository.getFullName());

            if (listener != null){
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onOpenPullRequests(position);
                    }
                });
            }

        }
    }

    public Repository getItem(int position){
        Repository repository = repositories.get(position);
        return repository;
    }

    public void setNotifyDataChanged(List<Repository> repositories){
        this.repositories = repositories;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return this.repositories.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.img_user_repository)
        ImageView imgUser;

        @Bind(R.id.txt_fork)
        TextView txtFork;

        @Bind(R.id.txt_star)
        TextView txtStar;

        @Bind(R.id.txt_title_repository)
        TextView txtTitle;

        @Bind(R.id.txt_description_repository)
        TextView txtDescription;

        @Bind(R.id.txt_username_user_repository)
        TextView txtUsername;

        @Bind(R.id.txt_name_user_repository)
        TextView txtName;
        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }

}

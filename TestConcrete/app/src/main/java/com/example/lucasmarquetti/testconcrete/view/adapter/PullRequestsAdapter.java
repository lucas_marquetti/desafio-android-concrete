package com.example.lucasmarquetti.testconcrete.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lucasmarquetti.testconcrete.R;
import com.example.lucasmarquetti.testconcrete.model.PullRequest;
import com.example.lucasmarquetti.testconcrete.utils.DateUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by LucasMarquetti on 12/11/16.
 */

public class PullRequestsAdapter extends RecyclerView.Adapter<PullRequestsAdapter.ViewHolder> {

    private Context context;
    private List<PullRequest> pullRequestList;
    private AdapterListener listener;

    public PullRequestsAdapter(Context context, AdapterListener listener, List<PullRequest> pullRequestList) {
        this.pullRequestList = pullRequestList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public PullRequestsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(context).inflate(R.layout.view_item_pull_request, parent, false);
        return new PullRequestsAdapter.ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(PullRequestsAdapter.ViewHolder holder, final int position) {
        PullRequest pullRequest = this.pullRequestList.get(position);

        if (pullRequest != null) {

            int sizeImage = (int) context.getResources().getDimension(R.dimen.item_icon_size);

            Picasso.with(context).
                    load(pullRequest.getUser().getAvatarUrl())
                        .placeholder(context.getResources().getDrawable(R.mipmap.ic_profile_gray))
                            .resize(sizeImage, sizeImage).into(holder.imgUser);

            holder.txtTitle.setText(pullRequest.getTitle());
            holder.txtBody.setText(pullRequest.getBody());
            holder.txtUsername.setText(pullRequest.getUser().getLogin());
            String date = DateUtil.dateToString(pullRequest.getCreateAt());

            holder.txtDateCreated.setText(date);
         //   holder.txtName.setText(pullRequest.getUser().);

            if (listener != null){
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onOpenPullRequests(position);
                    }
                });
            }
        }
    }

    public void setNotifyDataChanged(List<PullRequest> pullRequestList) {
        this.pullRequestList = pullRequestList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return this.pullRequestList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.img_user_pull)
        ImageView imgUser;

        @Bind(R.id.txt_title_pull)
        TextView txtTitle;

        @Bind(R.id.txt_description_pull)
        TextView txtBody;

        @Bind(R.id.txt_username_user_pull)
        TextView txtUsername;

        @Bind(R.id.txt_name_user_pull)
        TextView txtName;

        @Bind(R.id.txt_date_created_pull)
        TextView txtDateCreated;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
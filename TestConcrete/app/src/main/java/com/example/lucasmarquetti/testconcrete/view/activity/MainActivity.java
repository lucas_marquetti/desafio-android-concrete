package com.example.lucasmarquetti.testconcrete.view.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lucasmarquetti.testconcrete.R;
import com.example.lucasmarquetti.testconcrete.client.OnGithubRepositoriesListener;
import com.example.lucasmarquetti.testconcrete.client.api.ApiClient;
import com.example.lucasmarquetti.testconcrete.client.service.GithubImpl;
import com.example.lucasmarquetti.testconcrete.model.Repository;
import com.example.lucasmarquetti.testconcrete.view.EndlessRecyclerViewScrollListener;
import com.example.lucasmarquetti.testconcrete.view.adapter.AdapterListener;
import com.example.lucasmarquetti.testconcrete.view.adapter.RepositoryAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

public class MainActivity extends BaseActivity implements OnGithubRepositoriesListener, AdapterListener{

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.toolbar_title)
    TextView toolbarTitle;

    @Bind(R.id.recycler_repository)
    RecyclerView recyclerView;

    @Bind(R.id.view_empty)
    public LinearLayout emptyView;

    @Bind(R.id.empty_text)
    public TextView emptyTxt;

    private EndlessRecyclerViewScrollListener scrollListener;
    List<Repository> repositoryList = new ArrayList<>();
    RepositoryAdapter mAdapter;
    GithubImpl service;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setToolbar();
        getRepositories();
    }

    private void setToolbar(){
        toolbarTitle.setText(getString(R.string.app_name));
    }

    public void getRepositories() {
        showProgress();
        service = new GithubImpl();
        service.getListRespositories(this, this, 1);
    }

    public void createRecyclerView(){
        if (mAdapter == null){
            mAdapter = new RepositoryAdapter(this, this, repositoryList);

            final  LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

            scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
                @Override
                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                    showProgress();
                    service.getListRespositories(MainActivity.this, MainActivity.this, page);
                }
            };

            recyclerView.setAdapter(mAdapter);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.addOnScrollListener(scrollListener);
        }else {
            mAdapter.setNotifyDataChanged(repositoryList);
        }

    }

    @Override
    public void onListRepositories(List<Repository> listRepositories) {
        if (listRepositories == null || listRepositories.size() == 0){
            setEmptyView();
            return;
        }
        repositoryList.addAll(listRepositories);
        createRecyclerView();
        dismissProgress();
    }

    public void setEmptyView(){
        emptyTxt.setText(getString(R.string.txt_empty_repository));
        emptyView.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        dismissProgress();
    }

    @Override
    public void onListFail(String error) {
        setEmptyView();
    }

    @Override
    public void onOpenPullRequests(int position) {
        Repository repository = repositoryList.get(position);
        Intent intent = new Intent(this, PullRequestsActicity.class);
        intent.putExtra(PullRequestsActicity.INTENT_REPOSITORY, repository);

        startActivity(intent);
    }
}

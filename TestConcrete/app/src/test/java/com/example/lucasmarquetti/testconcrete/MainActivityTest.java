package com.example.lucasmarquetti.testconcrete;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.lucasmarquetti.testconcrete.client.service.GithubImpl;
import com.example.lucasmarquetti.testconcrete.model.Owner;
import com.example.lucasmarquetti.testconcrete.model.PullRequest;
import com.example.lucasmarquetti.testconcrete.model.Repository;
import com.example.lucasmarquetti.testconcrete.view.activity.MainActivity;
import com.example.lucasmarquetti.testconcrete.view.activity.PullRequestsActicity;
import com.example.lucasmarquetti.testconcrete.view.adapter.RepositoryAdapter;

import org.assertj.android.recyclerview.v7.api.Assertions;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
//import static org.assertj.android.recyclerview.v7.api.*;
import static org.junit.Assert.assertThat;

/**
 * Created by LucasMarquetti on 11/11/16.
 */


@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class MainActivityTest {

    private MainActivity activity;
    private RecyclerView recyclerView;

    @Before
    public void setUp() throws Exception
    {
        activity = Robolectric.buildActivity(MainActivity.class)
                .create()
                .resume()
                .get();

        recyclerView = (RecyclerView) activity.findViewById(R.id.recycler_repository);
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull( activity );
    }

    @Test
    public void shouldHaveRecyclerView() throws Exception
    {
        assertNotNull( activity.findViewById(R.id.recycler_repository));
    }

    @Test
    public void clickingToItemRecyclerView() throws Exception {

        Repository repository = getRepository();
        List<Repository> repositoryList = new ArrayList<>();
        repositoryList.add(repository);
        repositoryList.add(repository);

        activity.onListRepositories(repositoryList);
        RepositoryAdapter adapter = (RepositoryAdapter) recyclerView.getAdapter();


        activity.findViewById(R.id.recycler_repository).performClick();

        Intent expectedIntent = new Intent(activity, PullRequestsActicity.class);

        recyclerView.measure(0,0);
        recyclerView.layout(0,0,100,1000);

        recyclerView.findViewHolderForAdapterPosition(0).itemView.performClick();

        //assertThat(shadowOf(activity).getNextStartedActivity()).isEqualTo(expectedIntent);
    }

    @Test
    public void defaultDisplay() {
        activity.createRecyclerView();
        RecyclerView.LayoutManager layoutManager1 = recyclerView.getLayoutManager();

        activity.getRepositories();

        assertNotNull(layoutManager1);
    }


    //Now we make sure that the adapter is showing us our candies in the list.
    @Test
    public void onSuccessPopulateRecyclerView() {
        Repository repository = getRepository();
        List<Repository> repositoryList = new ArrayList<>();
        repositoryList.add(repository);
        repositoryList.add(repository);

        activity.onListRepositories(repositoryList);
        RepositoryAdapter adapter = (RepositoryAdapter) recyclerView.getAdapter();

        assertEquals(adapter.getItemCount(), 2);
        assertEquals(adapter.getItem(0), repository);

        repositoryList.add(repository);
        activity.onListRepositories(repositoryList);

        assertEquals(adapter.getItemCount(), 3);
    }


    @Test
    public void onFailPopulateRecyclerViewAppearEmptyView() {
        List<Repository> repositoryList = new ArrayList<>();
        activity.onListRepositories(repositoryList);

        int visibility = 0; // VISIBLE
        int visibility1 = activity.emptyView.getVisibility();

        assertEquals(visibility1, visibility);
        assertEquals(activity.emptyTxt.getText().toString(), "Não há repositórios no momento, tente novamente mais tarde.");
    }

    public Owner getOwner(){
        Owner owner = new  Owner("Stuart", 1, "www.google.com");

        return owner;
    }

    public Repository getRepository(){
        Repository repository = new Repository(232, "Elastic test", "Elastics Body",
                232, 555 , "Elastics Body Description",  getOwner());

        return repository;
    }
}

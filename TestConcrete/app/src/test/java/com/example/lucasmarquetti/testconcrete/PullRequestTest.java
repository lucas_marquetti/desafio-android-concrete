package com.example.lucasmarquetti.testconcrete;

import android.os.Bundle;
import android.os.Parcel;

import com.example.lucasmarquetti.testconcrete.model.Owner;
import com.example.lucasmarquetti.testconcrete.model.PullRequest;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * Created by LucasMarquetti on 11/11/16.
 */

public class PullRequestTest {

    @Test
    public void isValidPullRequestParcelable() throws Exception{

        PullRequest pullRequest = getPullRequest();

        assertEquals(pullRequest.getId(), 232);
        assertEquals(pullRequest.getTitle(), "Elastic test");
        assertEquals(pullRequest.getBody(), "Elastics Body");
        assertEquals(pullRequest.getNumber(), 232);
        assertEquals(pullRequest.getCreateAt(), "2016-11-12T22:12:58Z");
        assertNotNull(pullRequest.getUser());

    }

    @Test
    public void testParcelableInterface() throws Exception{

        PullRequest pullRequest = getPullRequest();

        Bundle bundle = new Bundle();
        bundle.putParcelable("pull", pullRequest);


        PullRequest parcelablePull = bundle.getParcelable("pull");

        assertEquals(pullRequest,  parcelablePull);

    }

    public Owner getOwner(){

        Owner owner = new  Owner("Stuart", 1, "www.google.com");

        return owner;
    }

    public PullRequest getPullRequest(){
        PullRequest pullRequest = new PullRequest(232,"https://github.com/elastic/elasticsearch/pull/21517",
                232, "Elastic test" , "Elastics Body", "2016-11-12T22:12:58Z", getOwner());

        return pullRequest;
    }
}

package com.example.lucasmarquetti.testconcrete;

import android.support.v7.widget.RecyclerView;

import com.example.lucasmarquetti.testconcrete.client.service.GithubImpl;
import com.example.lucasmarquetti.testconcrete.model.Owner;
import com.example.lucasmarquetti.testconcrete.model.PullRequest;
import com.example.lucasmarquetti.testconcrete.model.Repository;
import com.example.lucasmarquetti.testconcrete.view.activity.MainActivity;
import com.example.lucasmarquetti.testconcrete.view.activity.PullRequestsActicity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static org.junit.Assert.assertNotNull;

/**
 * Created by LucasMarquetti on 11/11/16.
 */

@RunWith(MockitoJUnitRunner.class)
public class PullRequestsActivityTest {

    @Mock private PullRequestsActicity activity;
    @Mock private GithubImpl githubService;

    private RecyclerView recyclerView;

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull(activity);
    }

    @Test
    public void shouldHaveRecyclerView() throws Exception
    {
        assertNotNull(activity.findViewById(R.id.recycler_pulls));
    }

    @Test
    public void getPullRequestsTest() throws Exception {

        activity.repository = getRepository();

//        InOrder inOrder = inOrder(activity);
////
////        inOrder.verify(activity).getPullRequests();
////        inOrder.verify(activity).showProgress();
////        inOrder.verify(activity).createRecyclerView();
////        inOrder.verify(activity).dismissProgress();

        verify(activity, never()).getPullRequests();
        verify(activity, never()).createRecyclerView();
        verify(activity, never()).dismissProgress();

    }

    public Repository getRepository(){
        Repository repository = new Repository(232, "elasticsearch", "Elastics Body",
                232, 555 , "Elastics Body Description",  getOwner());

        return repository;
    }

    public Owner getOwner(){
        Owner owner = new  Owner("elastic", 1, "www.google.com");

        return owner;
    }

    public PullRequest getPullRequest(){
        PullRequest pullRequest = new PullRequest(232,"https://github.com/elastic/elasticsearch/pull/21517",
                232, "Elastic test" , "Elastics Body", "2016-11-12T22:12:58Z", getOwner());

        return pullRequest;
    }
}
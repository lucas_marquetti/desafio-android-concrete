package com.example.lucasmarquetti.testconcrete;

import com.example.lucasmarquetti.testconcrete.utils.DateUtil;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by LucasMarquetti on 11/11/16.
 */

public class DateUtilTest {

    @Test
    public void isDateValid() throws Exception{

        assertEquals("12/11/16", DateUtil.dateToString("2016-11-12T22:12:58Z"));

        assertNotEquals("12/11/2016", DateUtil.dateToString("2016-11-12T22:12:58Z"));

    }
}
